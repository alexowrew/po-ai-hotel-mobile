import {NgModule} from "@angular/core";
import {Routes} from "@angular/router";
import {NativeScriptRouterModule} from "nativescript-angular/router";
import {AuthGuard} from "~/app/shared/guards/auth.guard";
import {DashboardComponent} from "~/app/screens/main/dashboard/dashboard.component";
import {ReservationsComponent} from "~/app/screens/main/reservations/reservations.component";
import {ReservationsAddComponent} from "~/app/screens/main/reservations-add/reservations-add.component";
import {ReservationsActiveComponent} from "~/app/screens/main/reservations-active/reservations-active.component";
import {LoginComponent} from "~/app/screens/login/login.component";

const routes: Routes = [
    {path: "", redirectTo: "/dashboard", pathMatch: "full"},
    {
        path: "",
        canActivate: [AuthGuard],
        loadChildren: "~/app/screens/main/main.module#MainModule"
    },
    {path: "login", loadChildren: "~/app/screens/login/login.module#LoginModule"}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {
}
