import {Component, OnInit, ViewChild} from "@angular/core";
import {DrawerTransitionBase, SlideInOnTopTransition} from "nativescript-ui-sidedrawer";
import {RadSideDrawerComponent} from "nativescript-ui-sidedrawer/angular";
import {NavigationEnd, Router} from "@angular/router";
import {RouterExtensions} from "nativescript-angular";
import {filter} from "rxjs/operators";
import {AuthenticationService} from "~/app/shared/services/authentication.service";
import {UserImmutable} from "~/app/shared/types";

@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html",
    styleUrls: ["app.scss"]
})
export class AppComponent implements OnInit {
    private _activatedUrl: string;
    private _sideDrawerTransition: DrawerTransitionBase;
    private _user: UserImmutable;

    @ViewChild("sideDrawer")
    sideDrawerComponent: RadSideDrawerComponent;

    constructor(private router: Router, private routerExtensions: RouterExtensions, private auth: AuthenticationService) {
    }

    ngOnInit(): void {
        this._activatedUrl = "/dashboard";
        this._sideDrawerTransition = new SlideInOnTopTransition();

        this.auth.user.subscribe(user => this._user = user);

        this.router.events
            .pipe(filter((event: any) => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
                const end = event.urlAfterRedirects.indexOf('?');
                this._activatedUrl = event.urlAfterRedirects.substr(0, end < 0 ? event.urlAfterRedirects.length : end);
            });
    }

    get user(): UserImmutable {
        return this._user;
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    isComponentSelected(url: string): boolean {
        return this._activatedUrl === url;
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        this.sideDrawerComponent.sideDrawer.closeDrawer();
    }

    onDrawerButtonTap(): void {
        this.sideDrawerComponent.sideDrawer.showDrawer();
    }
}
