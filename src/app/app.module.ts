import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {ConnectorService} from "~/app/shared/services/connector.service";
import {ErrorInterceptor} from "~/app/shared/interceptors/error.interceptor";

import {AipoClientService} from "~/app/shared/AipoApiSDK/aipo-client.service";
import {AuthenticationService} from "~/app/shared/services/authentication.service";
import {JwtInterceptor} from "~/app/shared/interceptors/jwt.interceptor";
import {NativeScriptUISideDrawerModule} from "nativescript-ui-sidedrawer/angular";
import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptFormsModule} from "nativescript-angular";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptUISideDrawerModule,
        HttpClientModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        AuthenticationService,
        ConnectorService,
        AipoClientService,
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule {
}
