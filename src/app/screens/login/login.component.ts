import {Component, OnInit} from '@angular/core';
import {Page} from "tns-core-modules/ui/page";
import {isUndefined} from "tns-core-modules/utils/types";

import {UserLoginDto} from "~/app/shared/dto/user-login.dto";
import {AuthenticationService} from "~/app/shared/services/authentication.service";
import {ResponseNoTokenException} from "~/app/shared/exceptions/response-no-token.exception";

import {ErrorResponse} from "~/app/shared/models/error-response";
import {AipoClientService} from "~/app/shared/AipoApiSDK/aipo-client.service";
import {LoginResponse} from "~/app/shared/AipoApiSDK/response/login.response";
import {RouterExtensions} from "nativescript-angular";

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    moduleId: module.id
})
export class LoginComponent implements OnInit {

    public processing: boolean = false;
    public user: UserLoginDto = new UserLoginDto();
    public error: string = null;

    constructor(private page: Page,
                private authService: AuthenticationService,
                private api: AipoClientService,
                private router: RouterExtensions) {
    }

    ngOnInit() {
        this.page.actionBarHidden = true;
    }

    submit() {
        this.error = null;
        this.processing = true;

        this.api.login(this.user).subscribe((response: LoginResponse) => {
            this.processing = false;

            if (isUndefined(response.token)) {
                throw new ResponseNoTokenException();
            }

            this.authService.login(AuthenticationService.fromToken(response.token));

            this.router.navigate(['/dashboard']);
        }, (error: ErrorResponse) => {
            this.processing = false;
            this.error = error.message;
        });
    }
}
