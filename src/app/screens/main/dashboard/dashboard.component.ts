import {Component, OnInit} from "@angular/core";
import {AuthenticationService} from "~/app/shared/services/authentication.service";
import {Page} from "tns-core-modules/ui/page";
import {UserImmutable} from "~/app/shared/types";

@Component({
    selector: "Dashboard",
    moduleId: module.id,
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {

    private _user: UserImmutable;

    constructor(private page: Page, private auth: AuthenticationService) {
        page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.auth.user.subscribe(user => this._user = user);
    }

    get user(): UserImmutable {
        return this._user;
    }
}
