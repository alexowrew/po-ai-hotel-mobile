import {NgModule} from "@angular/core";
import {Routes} from "@angular/router";
import {NativeScriptRouterModule} from "nativescript-angular/router";
import {DashboardComponent} from "~/app/screens/main/dashboard/dashboard.component";
import {ReservationsComponent} from "~/app/screens/main/reservations/reservations.component";
import {ReservationsAddComponent} from "~/app/screens/main/reservations-add/reservations-add.component";
import {ReservationsActiveComponent} from "~/app/screens/main/reservations-active/reservations-active.component";

const routes: Routes = [
    {path: "dashboard", component: DashboardComponent, pathMatch: 'full'},
    {path: "reservations", component: ReservationsComponent, pathMatch: 'full'},
    {path: "reservations/add", component: ReservationsAddComponent, pathMatch: 'full'},
    {path: "reservation", component: ReservationsActiveComponent, pathMatch: 'full'}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MainRoutingModule {
}
