import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {NativeScriptCommonModule} from "nativescript-angular/common";

import {MainRoutingModule} from "./main-routing.module";
import {DashboardComponent} from "~/app/screens/main/dashboard/dashboard.component";
import {ReservationsComponent} from "~/app/screens/main/reservations/reservations.component";
import {ReservationsAddComponent} from "~/app/screens/main/reservations-add/reservations-add.component";
import {ReservationsActiveComponent} from "~/app/screens/main/reservations-active/reservations-active.component";
import {NativeScriptFormsModule} from "nativescript-angular";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        MainRoutingModule
    ],
    declarations: [
        DashboardComponent,
        ReservationsComponent,
        ReservationsAddComponent,
        ReservationsActiveComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MainModule {
}
