import {Component, OnDestroy, OnInit} from "@angular/core";
import {AipoClientService} from "~/app/shared/AipoApiSDK/aipo-client.service";
import {ReservationCollection} from "~/app/shared/AipoApiSDK/types";
import {Reservation} from "~/app/shared/AipoApiSDK/models/reservation";
import {ErrorResponse} from "~/app/shared/models/error-response";

import * as dialogs from "tns-core-modules/ui/dialogs"
import {CheckOutResponse} from "~/app/shared/AipoApiSDK/response/check-out.response";
import {RouterExtensions} from "nativescript-angular";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";
import {Page} from "tns-core-modules/ui/page";
import {first} from "rxjs/operators";

@Component({
    selector: 'ActiveReservations',
    moduleId: module.id,
    templateUrl: "./reservations-active.component.html",
    styleUrls: ["./reservations-active.component.scss"]
})
export class ReservationsActiveComponent implements OnInit, OnDestroy {

    activeReservation: Reservation;
    isLoading: boolean = false;

    private subs: Subscription = new Subscription();

    constructor(private page: Page, private api: AipoClientService, private router: RouterExtensions) {
        page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.isLoading = true;
        const reservation = this.api.getReservations().subscribe((reservations: ReservationCollection) => {
            reservations.forEach((reservation: Reservation) => {
                if (reservation.isActive()) {
                    this.activeReservation = reservation;
                }
            });
            this.isLoading = false;
        });
        this.subs.add(reservation);
    }

    checkOut() {
        dialogs.confirm({
            title: 'Are you sure you want to check out?',
            okButtonText: 'Yes, check out',
            cancelButtonText: 'No'
        }).then(result => {
            if (result) {
                const checkOut = this.api.checkOut(this.activeReservation).pipe(first()).subscribe((checkoutResponse: CheckOutResponse) => {
                        dialogs.alert({
                            title: 'Check out successful',
                            message: 'You have been checked out successfully',
                            okButtonText: 'Ok'
                        }).then(() => {
                            this.activeReservation = null;
                            this.router.navigate(['/reservations'], {queryParams: {time: (new Date()).getTime()}});
                        });
                    },
                    (error: ErrorResponse) => {
                        dialogs.alert({
                            title: 'Check out error',
                            message: 'There has been an error during check out, please visit the lobby.'
                        })
                    });

                this.subs.add(checkOut);
            }
        });
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }
}
