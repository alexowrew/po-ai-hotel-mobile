import {Component, OnDestroy, OnInit} from "@angular/core";
import * as dialogs from "tns-core-modules/ui/dialogs"

import {AipoClientService} from "~/app/shared/AipoApiSDK/aipo-client.service";
import {ReservationDto} from "~/app/shared/AipoApiSDK/dto/reservation.dto";
import {ReservationPostResponse} from "~/app/shared/AipoApiSDK/response/reservation-post.response";
import {ErrorResponse} from "~/app/shared/models/error-response";
import {RouterExtensions} from "nativescript-angular";
import {Subscription} from "rxjs";
import {Page} from "tns-core-modules/ui/page";
import {first} from "rxjs/operators";

@Component({
    selector: 'Reservation',
    moduleId: module.id,
    templateUrl: "./reservations-add.component.html",
    styleUrls: ["./reservations-add.component.scss"]
})
export class ReservationsAddComponent implements OnInit, OnDestroy {

    reservation: ReservationDto = new ReservationDto();
    isBusy: boolean = false;
    floors: any = Array(10);

    private subs: Subscription = new Subscription();

    constructor(private page: Page, private api: AipoClientService, private router: RouterExtensions) {
        page.actionBarHidden = true;
    }

    ngOnInit(): void {
    }

    submit(): void {
        this.isBusy = true;

        const reservation = this.api.makeReservation(this.reservation).pipe(first()).subscribe((response: ReservationPostResponse) => {
                this.isBusy = false;
                if (response === null) {
                    return;
                }

                dialogs.alert({
                    title: 'Room has been booked',
                    message: 'Your booking has been saved',
                    okButtonText: 'Ok'
                }).then(() => {
                    this.router.navigate(['/reservations'], {queryParams: {time: (new Date()).getTime()}});
                });
            },
            (error: ErrorResponse) => {
                this.isBusy = false;

                switch (error.code) {
                    case 404:
                        dialogs.alert({
                            title: 'No room found',
                            message: 'No free room was found for given parameters',
                            okButtonText: 'Ok'
                        });
                        break;
                    default:
                        dialogs.alert({
                            title: 'Unknown error',
                            message: 'An unknown error has occurred',
                            okButtonText: 'Ok'
                        });
                }
            });

        this.subs.add(reservation);
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }


}
