import {AfterViewInit, Component, OnDestroy, OnInit} from "@angular/core";

import {AipoClientService} from "~/app/shared/AipoApiSDK/aipo-client.service";
import {ReservationCollection} from "~/app/shared/AipoApiSDK/types";
import * as dialogs from "tns-core-modules/ui/dialogs";
import {Reservation} from "~/app/shared/AipoApiSDK/models/reservation";
import {CheckInResponse} from "~/app/shared/AipoApiSDK/response/check-in.response";
import {ErrorResponse} from "~/app/shared/models/error-response";
import {RouterExtensions} from "nativescript-angular";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {Page} from "tns-core-modules/ui/page";
import {first} from "rxjs/operators";

@Component({
    selector: 'Reservations',
    moduleId: module.id,
    templateUrl: "./reservations.component.html",
    styleUrls: ["./reservations.component.scss"]
})
export class ReservationsComponent implements OnInit, OnDestroy {

    isLoading: boolean = false;
    reservations: ReservationCollection;
    hasActiveReservation: boolean = false;

    private subs: Subscription = new Subscription();

    constructor(private page: Page, private api: AipoClientService, private router: Router) {
        page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.isLoading = true;
        const getReservations = this.api.getReservations().subscribe(
            (response: ReservationCollection) => {
                this.isLoading = false;
                this.reservations = response;
                this.hasActiveReservation = false;
                response.forEach(reservation => {
                    if (reservation.isActive()) {
                        this.hasActiveReservation = true;
                    }
                });
            },
            () => {
                dialogs.alert({
                    title: 'Error when retrieving reservations',
                    message: 'There was an error while retrieving reservations, please try again later.',
                    okButtonText: 'Ok'
                });
            });

        this.subs.add(getReservations);
    }

    reservationInRange(reservation: Reservation) {
        const today = new Date();
        today.setHours(0, 0, 0, 0);
        return (reservation.dateStart.getTime() <= today.getTime()) && (reservation.dateEnd.getTime() >= today.getTime());
    }

    checkIn(reservation: Reservation) {
        const checkIn = this.api.checkIn(reservation).subscribe(
            (response: CheckInResponse) => {
                this.router.navigate(['/reservation'], {skipLocationChange: true, queryParams: {time: (new Date()).getTime()}})
            },


            (error: ErrorResponse) => {
                dialogs.alert({
                    title: 'Error ',
                    message: error.message,
                    okButtonText: 'Ok'
                });
            }
        );

        this.subs.add(checkIn);
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }

    canCheckIn(reservation: Reservation) {
        const isNotDone = !reservation.isDone;
        const hasNoActiveReservations = !this.hasActiveReservation;
        const dateIsInRange = this.reservationInRange(reservation);

        return isNotDone && hasNoActiveReservations && dateIsInRange;
    }
}
