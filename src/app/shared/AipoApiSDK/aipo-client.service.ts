import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable, ReplaySubject, Subject} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {environment} from "~/environment/environment";

import {ErrorResponse} from "~/app/shared/models/error-response";
import {Request} from "~/app/shared/models/request";
import {UserLoginDto} from "~/app/shared/dto/user-login.dto";
import {Url} from "~/app/shared/models/url";
import {HttpVerb} from "~/app/shared/enums/http-verb";
import {ConnectorService} from "~/app/shared/services/connector.service";

import {LoginResponse} from "~/app/shared/AipoApiSDK/response/login.response";

import {ReservationCollection, RoomCollection} from "~/app/shared/AipoApiSDK/types";
import {ReservationDto} from "~/app/shared/AipoApiSDK/dto/reservation.dto";
import {ReservationPostResponse} from "~/app/shared/AipoApiSDK/response/reservation-post.response";
import {Reservation} from "~/app/shared/AipoApiSDK/models/reservation";
import {JsonConvert} from "json2typescript";
import {CheckInResponse} from "~/app/shared/AipoApiSDK/response/check-in.response";
import {CheckOutResponse} from "~/app/shared/AipoApiSDK/response/check-out.response";

@Injectable()
export class AipoClientService {

    private _loginStream: ReplaySubject<ResponseInterface> = new ReplaySubject<ResponseInterface>(1);
    get loginStream() {
        return this._loginStream.asObservable();
    }

    private _roomStream: ReplaySubject<RoomCollection> = new ReplaySubject<RoomCollection>(1);
    get roomStream() {
        return this._roomStream.asObservable();
    }

    private _reservationStream: ReplaySubject<ReservationPostResponse> = new ReplaySubject<ReservationPostResponse>(0);
    get reservationStream() {
        return this._reservationStream.asObservable();
    }

    private _reservationsStream: ReplaySubject<ReservationCollection> = new ReplaySubject<ReservationCollection>(1);
    get reservationsStream() {
        return this._reservationsStream.asObservable();
    }

    private _checkInStream: Subject<CheckInResponse> = new Subject<CheckInResponse>();
    get checkInStream() {
        return this._checkInStream.asObservable();
    }

    private _checkOutStream: Subject<CheckInResponse> = new Subject<CheckInResponse>();
    get checkOutStream() {
        return this._checkOutStream.asObservable();
    }

    constructor(private client: ConnectorService) {
    }

    public login(data: UserLoginDto): Observable<ResponseInterface> {
        const request = new Request(
            HttpVerb.POST,
            new Url(environment.api.endpointUrl, '/login'),
            data
        );

        this.client.request<LoginResponse>(request).subscribe(
            (response: LoginResponse) => this._loginStream.next(response),
            (error: HttpErrorResponse) => {
                this._loginStream.error(new ErrorResponse(error))
            }
        );

        return this.loginStream;
    }

    public getRooms(): Observable<RoomCollection> {
        const request = new Request(
            HttpVerb.GET,
            new Url(environment.api.endpointUrl, '/rooms')
        );

        this.client.request<RoomCollection>(request).subscribe(
            (response: RoomCollection) => this._roomStream.next(response),
            (error: HttpErrorResponse) => this._roomStream.error(new ErrorResponse(error))
        );

        return this.roomStream;
    }

    public makeReservation(reservation: ReservationDto): Observable<ReservationPostResponse> {
        const request = new Request(
            HttpVerb.POST,
            new Url(environment.api.endpointUrl, '/reservation'),
            reservation
        );

        this.client.request<ReservationPostResponse>(request).subscribe(
            (response: ReservationPostResponse) => this._reservationStream.next(response || new ReservationPostResponse()),
            (error: HttpErrorResponse) => this._reservationStream.error(new ErrorResponse(error))
        );

        return this.reservationStream;
    }

    public getReservations(): Observable<ReservationCollection> {
        const request = new Request(
            HttpVerb.GET,
            new Url(environment.api.endpointUrl, '/reservations')
        );

        this.client.request<ReservationCollection>(request).subscribe(
            (response) => this._reservationsStream.next(response.map(item => (new JsonConvert()).deserializeObject(item, Reservation))),
            (error: HttpErrorResponse) => this._reservationsStream.error(new ErrorResponse(error))
        );

        return this.reservationsStream;
    }

    public checkIn(reservation: Reservation): Observable<CheckInResponse> {
        const request = new Request(
            HttpVerb.PUT,
            new Url(environment.api.endpointUrl, '/reservation/:reservationId/check-in', 'http', {
                ':reservationId': reservation.id
            })
        );


        this.client.request<CheckInResponse>(request).subscribe(
            (response: CheckInResponse) => this._checkInStream.next(response),
            (error: HttpErrorResponse) => this._checkInStream.error(new ErrorResponse(error))
        );

        return this.checkInStream;
    }

    public checkOut(reservation: Reservation): Observable<CheckOutResponse> {
        const request = new Request(
            HttpVerb.PUT,
            new Url(environment.api.endpointUrl, '/reservation/:reservationId/check-out', 'http', {
                ':reservationId': reservation.id
            })
        );

        this.client.request<CheckOutResponse>(request).subscribe(
            (response: CheckOutResponse) => this._checkOutStream.next(response),
            (error: HttpErrorResponse) => this._checkOutStream.error(new ErrorResponse(error))
        );

        return this.checkOutStream;
    }
}
