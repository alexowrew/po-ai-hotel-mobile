import {RoomStandard} from "~/app/shared/AipoApiSDK/types";
import {DtoInterface} from "~/app/shared/interfaces/dto.interface";

export class ReservationDto implements DtoInterface {
    floor: number = 0;
    standard: RoomStandard;
    dateStart: Date = new Date();
    dateEnd: Date = new Date();
    bedNo: number = 1;
    reservationDetails: string;
}
