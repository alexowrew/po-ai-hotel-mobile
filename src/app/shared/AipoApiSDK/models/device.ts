import {JsonObject, JsonProperty} from "json2typescript";

@JsonObject("Device")
export class Device {
    @JsonProperty("id", Number)
    id: number = undefined;
    @JsonProperty("name", String)
    name: string = undefined;
    @JsonProperty("status", Number)
    status: number = undefined;
}
