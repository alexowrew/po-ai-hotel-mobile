import {JsonObject, JsonProperty} from "json2typescript";

@JsonObject("Equipment")
export class Equipment {
    @JsonProperty("id", Number)
    id: number = undefined;
    @JsonProperty("name", String)
    name: string = undefined;
    @JsonProperty("status", Number)
    status: number = undefined;
}
