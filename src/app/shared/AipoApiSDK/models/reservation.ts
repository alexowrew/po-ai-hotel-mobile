import {Room} from "~/app/shared/AipoApiSDK/models/room";
import {User} from "~/app/shared/AipoApiSDK/models/user";
import {JsonConverter, JsonCustomConvert, JsonObject, JsonProperty} from "json2typescript";

@JsonConverter
class DateConverter implements JsonCustomConvert<Date> {
    serialize(date: Date): any {
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    }

    deserialize(date: string): Date {
        return new Date(Date.parse(date));
    }
}

@JsonObject("Reservation")
export class Reservation {

    @JsonProperty("id", Number)
    id: number = undefined;

    @JsonProperty("user", User)
    user: User = undefined;

    @JsonProperty("token", String)
    token: string = undefined;

    @JsonProperty("reservationDetails", String, true)
    reservationDetails: string = undefined;

    @JsonProperty("room", Room)
    room: Room = undefined;

    @JsonProperty("dateStart", DateConverter)
    dateStart: Date = undefined;

    @JsonProperty("dateEnd", DateConverter)
    dateEnd: Date = undefined;

    @JsonProperty("checkInDate", DateConverter, true)
    checkInDate: Date = undefined;

    @JsonProperty("checkOutDate", DateConverter, true)
    checkOutDate: Date = undefined;

    isActive() {
        return this.checkInDate && !this.checkOutDate;
    }

    get roomNumber(){
        return this.room.floor + '' + (this.room.number.toString().length === 1 ? '0' + this.room.number : this.room.number);
    }

    get isDone() {
        return !!this.checkOutDate;
    }

    get formattedDateEnd() {
        return this.dateEnd.getDate() + '/' + (1 + this.dateEnd.getMonth()) + '/' + this.dateEnd.getFullYear();
    }

    get formattedDateStart() {
        return this.dateStart.getDate() + '/' + (1 + this.dateEnd.getMonth()) + '/' + this.dateStart.getFullYear();
    }

    get formattedCheckinDate() {
        return this.checkInDate.getDate() + '/' + (1 + this.dateEnd.getMonth()) + '/' + this.checkInDate.getFullYear();
    }

    get formattedCheckoutDate() {
        return this.checkOutDate.getDate() + '/' + (1 + this.dateEnd.getMonth()) + '/' + this.checkOutDate.getFullYear();
    }
}
