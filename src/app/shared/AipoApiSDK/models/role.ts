import {JsonObject, JsonProperty} from "json2typescript";

@JsonObject("Role")
export class Role {
    @JsonProperty("id", Number)
    id: number = undefined;
    @JsonProperty("role", String)
    role: string = undefined;
    @JsonProperty("roleLabel", String)
    roleLabel: string = undefined;
}
