import {Equipment} from "~/app/shared/AipoApiSDK/models/equipment";
import {Device} from "~/app/shared/AipoApiSDK/models/device";
import {Reservation} from "~/app/shared/AipoApiSDK/models/reservation";
import {JsonObject, JsonProperty} from "json2typescript";

@JsonObject("Room")
export class Room {
    @JsonProperty("id", Number)
    id: number = undefined;
    @JsonProperty("number", Number)
    number: number = undefined;
    @JsonProperty("floor", Number)
    floor: number = undefined;
    @JsonProperty("equipment", [Equipment])
    equipment: Array<Equipment> = undefined;
    @JsonProperty("devices", [Device])
    devices: Array<Device> = undefined;
    // @JsonProperty("reservations", [Reservation])
    // reservations: Array<Reservation> = undefined;
}
