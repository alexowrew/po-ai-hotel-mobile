import {Reservation} from "~/app/shared/AipoApiSDK/models/reservation";
import {Role} from "~/app/shared/AipoApiSDK/models/role";
import {JsonObject, JsonProperty} from "json2typescript";

@JsonObject("User")
export class User {
    @JsonProperty("email", String)
    email: string = undefined;
    @JsonProperty("id", Number)
    id: number = undefined;
    // @JsonProperty("username", String)
    username: string = undefined;
    // @JsonProperty("password", String)
    password: string = undefined;
    // @JsonProperty("salt", String)
    salt: string = undefined;
    @JsonProperty("roles", [Role])
    roles: Array<Role> = undefined;
    // @JsonProperty("reservations", [Reservation])
    // reservations: Array<Reservation> = undefined;
}
