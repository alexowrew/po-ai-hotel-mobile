export class LoginResponse implements ResponseInterface {
    private _token: string;

    get token(): string {
        return this._token;
    }

    set token(value: string) {
        this._token = value;
    }
}
