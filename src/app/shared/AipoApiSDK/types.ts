import {Room} from "~/app/shared/AipoApiSDK/models/room";
import {Reservation} from "~/app/shared/AipoApiSDK/models/reservation";

export enum RoomStandard {
    Economy = 1,
    Premium = 2,
    Deluxe = 3
}

export enum Resource {
    Room,
    Reservation
}

export type ReservationCollection = Array<Reservation>;
export type RoomCollection = Array<Room>;
