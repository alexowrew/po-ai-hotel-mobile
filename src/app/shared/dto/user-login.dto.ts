import {DtoInterface} from "~/app/shared/interfaces/dto.interface";

export class UserLoginDto implements DtoInterface {
    public password: string;
    public username: string;
}
