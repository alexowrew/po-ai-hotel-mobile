export class Base64Helper {
    public static dict = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    static encode(value: string) {
        let char1: number,
            char2: number,
            char3: number;
        let padding: string = "";
        let paddingLength: number = value.length % 4;
        let chars = [];
        let result: string = "";

        if (paddingLength > 0) {
            padding = '='.repeat(paddingLength);
            value += "\x00".repeat(paddingLength);
        }

        let bit24;
        for (let i = 0; i < value.length; i += 3) {
            char1 = value.charCodeAt(i);
            char2 = value.charCodeAt(i + 1);
            char3 = value.charCodeAt(i + 2);

            if (isNaN(char2)) {
                bit24 = (char1 << 16);
            } else if (isNaN(char3)) {
                bit24 = (char1 << 16) + (char2 << 8);
            } else {
                bit24 = (char1 << 16) + (char2 << 8) + char3;
            }

            chars = [(bit24 >>> 18) & 63, (bit24 >>> 12) & 63, (bit24 >>> 6) & 63, bit24 & 63];

            result += Base64Helper.dict.charAt(chars[0]) +
                Base64Helper.dict.charAt(chars[1]) +
                (isNaN(char2) ? "" : Base64Helper.dict.charAt(chars[2])) +
                (isNaN(char3) ? "" : Base64Helper.dict.charAt(chars[3]));
        }

        return result.substring(0, result.length - paddingLength) + padding;
    }

    static decode(value: string) {
        value = value.replace(new RegExp('[^' + Base64Helper.dict.split("") + '=]', 'g'), "");

        const paddingLength = value.length % 4;
        let result = "";

        value += "A".repeat(paddingLength);

        const flippedDict = {};
        const dictArray = Base64Helper.dict.split("");
        for (let i in dictArray) {
            flippedDict[dictArray[i]] = parseInt(i);
        }

        for (let c = 0; c < value.length; c += 4) {
            let bit24 = (flippedDict[value[c]] << 18) + (flippedDict[value[c + 1]] << 12) +
                (flippedDict[value[c + 2]] << 6) + flippedDict[value[c + 3]];

            result += String.fromCharCode((bit24 >>> 16) & 255, (bit24 >>> 8) & 255, bit24 & 255);
        }

        return result.substring(0, result.length - paddingLength);
    }

    static _utf8_encode(value: string) {
        value = value.replace(/\r\n/g, "n");
        let result = "";
        for (let n = 0; n < value.length; n++) {
            let charCode = value.charCodeAt(n);
            if (charCode < 128) {
                result += String.fromCharCode(charCode)
            } else if (charCode > 127 && charCode < 2048) {
                result += String.fromCharCode(charCode >> 6 | 192);
                result += String.fromCharCode(charCode & 63 | 128)
            } else {
                result += String.fromCharCode(charCode >> 12 | 224);
                result += String.fromCharCode(charCode >> 6 & 63 | 128);
                result += String.fromCharCode(charCode & 63 | 128)
            }
        }

        return result;
    }

    static _utf8_decode(value: string) {
        let result = "";
        let charIdx = 0;
        let charCode = 0;
        let c2 = 0;
        let c3 = 0;
        while (charIdx < value.length) {
            charCode = value.charCodeAt(charIdx);
            if (charCode < 128) {
                result += String.fromCharCode(charCode);
                charIdx++
            } else if (charCode > 191 && charCode < 224) {
                c2 = value.charCodeAt(charIdx + 1);
                result += String.fromCharCode((charCode & 31) << 6 | c2 & 63);
                charIdx += 2
            } else {
                c2 = value.charCodeAt(charIdx + 1);
                c3 = value.charCodeAt(charIdx + 2);
                result += String.fromCharCode((charCode & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                charIdx += 3
            }
        }

        return result;
    }
}
