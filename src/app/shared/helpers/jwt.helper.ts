import {Base64Helper} from "~/app/shared/helpers/base64.helper";

export class JwtHelper {
    static SEPARATOR = '.';

    static decode(token: string): { [p: string]: string } {
        try {
            return JSON.parse(Base64Helper.decode(token.split(JwtHelper.SEPARATOR)[1]));
        } catch (e) {
            return null;
        }
    }

    static isTokenExpired(token: string): boolean {
        const data = JwtHelper.decode(token);

        if (data === null) {
            return true;
        }

        const expirationDate = new Date(parseInt(data.exp) * 1000);

        return expirationDate < new Date();
    }
}
