import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs';

import {AuthenticationService} from "~/app/shared/services/authentication.service";
import {UserImmutable} from "~/app/shared/types";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    private _user: UserImmutable;

    constructor(private authService: AuthenticationService) {
        this.authService.user.subscribe(user => this._user = user);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this._user && this._user.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this._user.token}`
                }
            });
        }

        return next.handle(request);
    }
}
