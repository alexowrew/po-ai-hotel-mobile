import {Observable} from "rxjs";

export interface ConnectorInterface {
    request(IRequest): Observable<ResponseInterface>;
    requestAuthorized(IRequest): Observable<ResponseInterface>;
}
