import {HttpVerb} from "~/app/shared/enums/http-verb";
import {UrlInterface} from "~/app/shared/interfaces/url.interface";
import {DtoInterface} from "~/app/shared/interfaces/dto.interface";

export interface RequestInterface {
    getMethod(): HttpVerb;

    getUrl(): UrlInterface;

    getData(): DtoInterface;
}
