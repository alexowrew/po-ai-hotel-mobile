import {HttpErrorResponse} from "@angular/common/http";

export class ErrorResponse implements ResponseInterface {
    private readonly _message: string;
    private readonly _code: number;

    constructor(error: HttpErrorResponse) {
        this._code = error.status;
        this._message = error.message;
    }

    get message() {
        return this._message;
    }

    get code() {
        return this._code;
    }
}
