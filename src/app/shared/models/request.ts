import {RequestInterface} from "~/app/shared/interfaces/request.interface";
import {HttpVerb} from "~/app/shared/enums/http-verb";
import {UrlInterface} from "~/app/shared/interfaces/url.interface";
import {DtoInterface} from "~/app/shared/interfaces/dto.interface";

export class Request implements RequestInterface {

    private readonly _method: HttpVerb;
    private readonly _url: UrlInterface;
    private readonly _data: DtoInterface;

    constructor(method: HttpVerb, url: UrlInterface, data?: DtoInterface) {
        this._method = method;
        this._url = url;
        this._data = data;
    }

    getData(): DtoInterface {
        return this._data;
    }

    getMethod(): HttpVerb {
        return this._method;
    }

    getUrl(): UrlInterface {
        return this._url;
    }

}
