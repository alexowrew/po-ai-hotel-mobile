import {UrlInterface} from "~/app/shared/interfaces/url.interface";

export class Url implements UrlInterface {

    private readonly resource: string;
    private readonly path: string = '/';
    private readonly protocol: string = 'http';
    private readonly pathParams: any = {};
    private readonly queryString: string | { [key: string]: string } = '';
    private readonly basicAuthentication: string = '';

    constructor(resource: string, path: string = '/', protocol: string = 'http', pathParams: any = {}, queryString: string | { [p: string]: string } = '', basicAuthentication: string = '') {
        this.resource = resource;
        this.path = path;
        this.protocol = protocol;
        this.pathParams = pathParams;
        this.queryString = queryString;
        this.basicAuthentication = basicAuthentication;
    }

    toString(): string {
        let queryString = null;
        if (typeof this.queryString === 'object') {
            queryString = Url.buildQueryString(this.queryString);
        } else {
            queryString = this.queryString;
        }

        let path = this.path;
        if (Object.keys(this.pathParams).length > 0) {
            for (let key in this.pathParams) {
                path = this.path.replace(key, this.pathParams[key]);
            }
        }

        if (this.protocol === '' || this.resource === '') {
            return `${this.resource}${path || ''}${queryString ? '?' + queryString : ''}`;
        }

        return `${this.protocol + '://' || ''}${this.basicAuthentication || ''}${this.resource}${path || ''}${queryString ? '?' + queryString : ''}`;
    }

    public static buildQueryString(elements: object) {
        let qs: string = '';
        for (let i in elements) {
            if (typeof elements[i] !== 'string') {
                throw new Error('Invalid query parameter type.');
            }

            if (encodeURIComponent(elements[i]) !== '') {
                qs += (qs !== '' ? '&' : '') + encodeURIComponent(i) + '=' + encodeURIComponent(elements[i]);
            }
        }

        return qs;
    }
}
