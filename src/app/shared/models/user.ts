import {JwtTokenExpiredException} from "~/app/shared/exceptions/jwt-token-expired.exception";
import {JwtHelper} from "~/app/shared/helpers/jwt.helper";

export class User {

    private readonly _email;
    private readonly _token: string;
    private readonly _roles;


    constructor(email, roles, token) {
        this._email = email;
        this._roles = roles;
        this._token = token;
    }

    get email() {
        return this._email;
    }

    get token() {
        return this._token;
    }

    static fromToken(token: string) {
        if (JwtHelper.isTokenExpired(token)) {
            throw new JwtTokenExpiredException();
        }

        const data = JwtHelper.decode(token);
        return new User(data.username, data.roles, token);
    }
}
