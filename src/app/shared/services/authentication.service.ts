import {Injectable} from "@angular/core";
import * as ApplicationSettings from "tns-core-modules/application-settings"

import {User} from "~/app/shared/models/user";
import {UserImmutable} from "~/app/shared/types";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class AuthenticationService {

    static readonly KEY_TOKEN_OBJECT = 'user_token';
    private _user: BehaviorSubject<UserImmutable>;

    constructor() {
        this.autologin();

        this.user.subscribe((user: UserImmutable) => {
            if (user === null) {
                AuthenticationService.destroyToken();
            } else {
                AuthenticationService.saveToken(user.token);
            }
        });
    }

    get user(): Observable<UserImmutable> {
        return this._user.asObservable();
    }

    login(user: User) {
        this._user.next(Object.freeze(user));
    }

    logout() {
        this._user.next(null);
    }

    static saveToken(token: string) {
        ApplicationSettings.setString(AuthenticationService.KEY_TOKEN_OBJECT, token);
    }

    static loadToken() {
        return ApplicationSettings.getString(AuthenticationService.KEY_TOKEN_OBJECT, null);
    }

    static destroyToken() {
        ApplicationSettings.remove(AuthenticationService.KEY_TOKEN_OBJECT);
    }

    static fromToken(token: string): User {
        return User.fromToken(token);
    }

    private autologin() {
        const token: string = AuthenticationService.loadToken();
        let user = null;
        try {
            if (token) {
                user = AuthenticationService.fromToken(token);
            }
        } catch (e) {

        }

        this._user = new BehaviorSubject<UserImmutable>(user);
    }
}
