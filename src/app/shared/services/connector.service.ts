import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

import {ConnectorInterface} from "~/app/shared/interfaces/connector.interface";
import {RequestInterface} from "~/app/shared/interfaces/request.interface";

import {HttpVerb} from "~/app/shared/enums/http-verb";
import {UnknownVerbException} from "~/app/shared/exceptions/unknown-verb.exception";
import {map} from "rxjs/operators";

@Injectable()
export class ConnectorService implements ConnectorInterface {
    constructor(private http: HttpClient) {
    }

    requestAuthorized<T extends ResponseInterface>(request: RequestInterface): Observable<ResponseInterface> {
        return this.http.post<T>(<string>request.getUrl(), request.getData());
    }

    request<T>(request: RequestInterface): Observable<T> {
        switch (request.getMethod()) {
            case HttpVerb.GET:
                return this.get<T>(request);
            case HttpVerb.POST:
                return this.post<T>(request);
            case HttpVerb.PUT:
                return this.put<T>(request);
            case HttpVerb.DELETE:
                return this.delete<T>(request);
        }

        throw new UnknownVerbException();
    }

    private get<T extends ResponseInterface>(request: RequestInterface): Observable<T> {
        return this.http.get<T>(request.getUrl().toString());
    }

    private post<T extends ResponseInterface>(request: RequestInterface): Observable<T> {
        return this.http.post<T>(request.getUrl().toString(), request.getData());
    }

    private put<T extends ResponseInterface>(request: RequestInterface): Observable<T> {
        return this.http.put<T>(request.getUrl().toString(), request.getData());
    }

    private delete<T extends ResponseInterface>(request: RequestInterface): Observable<T> {
        return this.http.delete<T>(request.getUrl().toString());
    }
}
