import {User} from "~/app/shared/models/user";

export type UserImmutable = Readonly<User>;
