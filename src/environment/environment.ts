export const environment = {
    'api': {
        'endpointUrl': 'aipo.elitsum.com/v1',
        'endpoints': {
            'login': '/login'
        }
    }
};
