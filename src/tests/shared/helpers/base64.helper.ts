import {Base64Helper} from "~/app/shared/helpers/base64.helper.js";
import {assert} from "chai";

// A sample Mocha test
describe('Array', function () {
    describe('#indexOf()', function () {
        it('should return {"asdf": 123} when the value is eyJhc2RmIjogMTIzfQ==', function () {
            const encoded = "eyJhc2RmIjogMTIzfQ==";
            const json = '{"asdf": 123}';
            assert.equal(encoded, Base64Helper.decode(json));
        });
    });
});
